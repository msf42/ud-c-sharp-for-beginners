# 04: Non-Primitive Types

## 26: Introduction

Just basic intro

---

## 27: Classes

Classes combine related variables (fields) and functions (methods).

![image](images/Lesson027.png)

- As in Python, an **object** is an instance of a **class**
- `Void` as the *return type* means that it does not return any value, as in

```c#
public void Introduce()
```

- The `()` means it does not take any parameters

For example:

```c#
public class Calculator
{
  public int Add(int a, int b)
  {
    return a + b;
  }
}
```

- This method has a *return type* of `int`, and it takes the *parameters* `a` and `b`

### Creating Objects

```c#
int number;

var person = new Person();
person.Name = "Mosh";
person.Introduce();
```

### Static Modifier

- If we add `static` to `public static int Add(int a, int b)` it will allow us to access that method via the Calculator class.
- It makes the `add` method only exist in one place

---

## 28: Demo: Classes

- Keep 1 class per .cs file

### Program.cs

```c#
using System;
using _28.Math;

namespace _28
{
    class Program
  {
    static void Main(string[] args)
    {
      var john = new Person();
      john.FirstName = "John";
      john.LastName = "Smith";
      john.Introduce();

      Calculator calculator = new Calculator();
      var result = calculator.Add(1, 2);
      Console.WriteLine(result);
    }
  }
}
```

### Person.cs

```c#
using System;

namespace _28 // same namespace as Program.cs, so we don't have to import it
{
  public class Person
  {
    public string FirstName;
    public string LastName;
    public void Introduce()
    {
      System.Console.WriteLine("My name is " + FirstName + " " + LastName);
    }
  }
}
```

### Math/Calculator.cs

```c#
namespace _28.Math
{
  public class Calculator
  {
    public int Add(int a, int b)
    {
      return a + b;
    }
  }
}
```

### Output

```c#
/*
My name is John Smith
3
*/
```

---

## 29: Structs

The same as Classes, but use slightly less memory. Only use if you are creating thousands of them, and for simple things like an X/Y coordinate, or an RGB color definition.

![image](images/Lesson029.png)

---

## 30: Arrays

### What is an Array?

A data structure to store a collection of variables of the same type

### Declaring an Array

- Instead of

```c#
int number1;
int number2;
int number3;
```

- You can

```c#
int[] numbers = new int[3];
```

- Arrays are a fixed size, and memory must be allocated for them.

```c#
numbers[0] = 1;
numbers[1] = 2;
numbers[2] = 3;
```

If you know ahead of time the values you want, you can

```c#
int[] numbers = new int[3] {1, 2, 3};
```

---

## 31: Demo: Arrays

```c#
using System;

namespace _31
{
  class Program
  {
    static void Main(string[] args)
    {
      var numbers = new int[3];
      numbers[0] = 1;
      System.Console.WriteLine(numbers[0]); //=> 1
      System.Console.WriteLine(numbers[1]); //=> 0
      System.Console.WriteLine(numbers[2]); //=> 0

      var flags = new bool[3];
      flags[0] = true;
      System.Console.WriteLine(flags[0]); //=> True
      System.Console.WriteLine(flags[1]); //=> False
      System.Console.WriteLine(flags[2]); //=> False

      var names = new string[3] { "Jack", "John", "Mary"};

    }
  }
}

```

---

## 32: Strings

```c#
string firstname = "Steve";

string name = firstName + " " + lastName;
```

### string format

```c#
string name = string.Format("{0} {1}", firstName, lastName);

var numbers = new int[3] {1, 2, 3};
string list = string.Join(",", numbers);

string name = "Steve";
char firstChar = name[0];
```

- strings are immutable, you can make a new one, but not modify

### Escaping Characters

- \n = New Line
- \t = Tab
- \\ = Backslash
- \’ = Single quote
- \” = Double Quote

### Verbatim Strings

```c#
string path = "c:\\projects\\project1\\folder1";

// use @ sign instead
string path @"c:\projects\project1\folder1";
```

---

## 33: Demo: Strings

```c#
using System;
namespace _33
{
  class Program
  {
    static void Main(string[] args)
    {
      var firstName = "Mosh";
      var lastName = "Hamedani";
      var fullName = firstName + " " + lastName;
      var myFullName = string.Format("My name is {0} {1}", firstName, lastName);
      System.Console.WriteLine(myFullName);
      // My name is Mosh Hamedani

      var names = new string[3] {"John", "Jack", "Mary"};
      var formattedNames = string.Join(",", names);
      System.Console.WriteLine(formattedNames);
      // John,Jack,Mary

      var text1 = "Hi John\nLook into the following paths\nc:\\folder1\\folder2\nc:\\folder3\\folder4";

      var text2 = @"Hi John
      Look into the following paths
      c:\\folder1\\folder2
      c:\\folder3\\folder4";
      System.Console.WriteLine(text1);
      /*
      Hi John
      Look into the following paths
      c:\folder1\folder2
      c:\folder3\folder4
      */

      System.Console.WriteLine(text2);
      /*
      Hi John
      Look into the following paths
      c:\\folder1\\folder2
      c:\\folder3\\folder4
      */
    }
  }
}
```

---

## 34: Enums

- An enum represents a set of name/value pairs (constants)
- Declare an enum instead of multiple constants.

### Use enums when you have multiple related constants

- Instead of

```c#
const int RegularAirMail = 1;
const int RegisteredAirMail = 2;
const int Express = 3;
```

- Do this

```c#
public enum ShippingMethod :byte // integer by default, use :type to change that
{
  RegularAirMail = 1,
  RegisteredAirMail = 2,
  Express = 3;
}
```

```c#
var method = ShippingMethod.Express;
```

---

## 35: Demo: Enums

```c#
using System;
namespace _35

{
  public enum ShippingMethod
  {
    RegularAirMail = 1,
    RegisteredAirMail = 2,
    Express = 3
  }

  class Program
  {
    static void Main(string[] args)
    {
      var method = ShippingMethod.Express;
      Console.WriteLine((int)method);
      // 3

      var methodId = 3;
      Console.WriteLine((ShippingMethod)methodId);
      // Express

      Console.WriteLine(method.ToString());
      // Express
      // Console.WriteLine always converts to string anyway

      var methodName = "Express";
      // parse = convert to a different type
      var shippingMethod = (ShippingMethod)Enum.Parse(typeof(ShippingMethod), methodName);
      System.Console.WriteLine(shippingMethod);  
      // Express
    }
  }
}
```

---

## 36: Reference Types and Value Types

- Structures: Value Types
- Classes: Reference Types

![image](images/Lesson036.png)

---

## 37: Demo: Reference Types and Value Types

```c#
using System;

namespace _37
{
  class Program
  {
    static void Main(string[] args)
    {
      var a = 10;
      var b = a;
      b++;
      Console.WriteLine(string.Format("a: {0}, b:{1}", a, b));
      // output: a: 10, b:11
      // because integers are value types, var b is a copy

// an array is a reference type, so array2 just points to the same array
      var array1 = new int[3]{1, 2, 3};
      var array2 = array1;
      array2[0] = 0;
      Console.WriteLine(string.Format("array1: {0}, array2: {1}", array1[0], array2[0]));
      // output: array1: 0, array2: 0
      // because arrays are reference types, both are changed
    }
  }
}
```

- Since a variable is a Value Type, a copy is made, independent of the original variable.
- An array is a Reference Type, so the “copy” is just a new reference that points to the same array. If one is changed, they are both changed.

![image](images/Lesson037.png)

---

## 38: Demo: Reference Types and Value Types

```c#
using System;

namespace _38
{
  public class Person
  {
    public int Age;
  }
  
  class Program
  {
    static void Main(string[] args)
    {
      var number = 1;
      Increment(number); // creates, then destroys.. it is a copy
      Console.WriteLine(number); // number is still 1

      var person = new Person() {Age = 20}; // since this is a reference type
      MakeOld(person);  // person will now be 30
      Console.WriteLine(person.Age); // 30
    }
    public static void Increment(int number)
    {
      number += 10;
    }
    public static void MakeOld(Person person)
    {
      person.Age += 10;
    }
  }
}
```

---

## Quiz 3: Non-Primitive Types

Done

---

## 39: Summary

 [Summary.pdf](images\Summary-Non-Primitive-Types.pdf) 

---
