# 02: Introduction to C# and .NET Framework

---

## 06: Introduction

Just a basic intro to the section

---

## 07: C# vs .NET

- C# is a programming language
- .NET is a framework for building applications on Windows
- .NET consists of two components
  - CLR (Common Language Runtime)
  - Class Library
    - for building applications

---

## 08: What is CLR?

- Originally, there was **C** and **C++**
- For each of these, the code was written, then the compiler would translate it into whatever language the machine used that the code was being run on
- Microsoft borrowed a concept from Java when developing .NET
  - The compiler translates the code into an **IL** or Intermediate Language
- It is the job of **CLR** to translate the IL code into machine code
  - This is called **Just-in-time Compilation (JIT)**
- As long as your machine has CLR, you can run .NET applications

---

## 09: Architecture of .NET Applications

- At the highest level, your code is made of a group of classes. These classes collaborate with each other at runtime, and produce the results

![image](images/Lesson009.png)

- A **Class** is made of of
  - **Data (a.k.a. Attributes)**
  - **Methods (a.k.a. Functions)**
- Example: If the Class is `Car`
  - Data includes: `make`, `model`, `color`
  - Methods include: `start()`, `move()`
- A **Namespace** is a container for related classes
- An **Assembly (DLL or EXE)** is a container for related Namespaces

![image](images/Lesson009b.png)

- An **Application** consists of one or more assemblies.

![image](images/Lesson009c.png)

---

## 10: Getting Visual Studio

Done

---

## 11: Our First C# Application

**Console Application** => Does not have a graphical user interface.

```c#
using System;

namespace _11
{
  class Program
  {
    static void Main(string[] args) // main function, where everything starts
    // output Main(input)
    {
      Console.WriteLine("Hello World!");
    }
  }
}
```

---

## 12: What is ReSharper?

A commercial add-on that is really fucking overpriced.

---

## 13: Summary

- So in this section, you learned the basics of C#.

### C# vs .NET

- C# is a programming language, while .NET is a framework. It consists of a run-time environment (CLR) and a class library that we use for building applications.

### CLR

- When you compile an application, C# compiler compiles your code to IL (Intermediate Language) code. IL code is platform agnostics, which makes it possible to a take a C# program on a different computer with different hardware architecture and operating system and run it. For this to happen, we need CLR. When you run a C# application, CLR compiles the IL code into the native machine code for the computer on which it is running. This process is called Just-in-time Compilation (JIT).

### Architecture of .NET Applications

- In terms of architecture, an application written with C# consists of building blocks called classes. A class is a container for data (attributes) and methods (functions). Attributes represent the state of the application. Methods include code. They have logic. That's where we implement our algorithms and write code.
- A namespace is a container for related classes. So as your application grows in size, you may want to group the related classes into various namespaces for better maintainability.
- As the number of classes and namespaces even grow further, you may want to physically separate related namespaces into separate assemblies. An assembly is a file (DLL or EXE) that contains one or more namespaces and classes. An EXE file represents a program that can be executed. A DLL is a file that includes code that can be re-used across different programs.
In the next section, you'll learn about basics of the C# language, including variables, constants, type conversion and operators.

---
