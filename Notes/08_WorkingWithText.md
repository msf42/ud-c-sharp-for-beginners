# 08: Working with Text

## 62: Introduction

Done

---

## 63: Strings

### Useful Methods

```c#
ToUpper() // HELLO WORLD
ToLower() // hello world
Trim() // trims ends

IndexOf('a') // returns index of first 'a'
LastIndexOf('Hello') // returns last index of 'Hello'

Substring(startIndex) // everything from startIndex to end
Substring(startIndex, length) // from startIndex for length

Replace('this text', 'with this text')

String.IsNullOrEmpty(str)
String.IsNullOrWhiteSpace(str)

str.Split(' ') // splits by spaces, returns an array
```

### Converting a String to a Number

```c#
string s = "1234";

int i = int.parse(s);

int j = convert.ToInt32(s); // preferred
```

### Converting a Number to a String

```c#
int i = 1234;

string s = i.ToString();

string t = i.ToString(C); // $1,234.00

string t = i.ToString(C0); // $1,234
```

![image](images/Lesson063.png)

---

## 64: Strings: Demo

```c#
using System;

namespace _64
{
  class Program
  {
    static void Main(string[] args)
    {
      var fullName = "Mosh Hamedani ";
      System.Console.WriteLine("Trim: '{0}'", fullName.Trim());
      // Trim: 'Mosh Hamedani'
      System.Console.WriteLine("ToUpper: '{0}'", fullName.ToUpper());
      // ToUpper: 'MOSH HAMEDANI '

      var index = fullName.IndexOf(' ');
      var firstName = fullName.Substring(0, index);
      var lastName = fullName.Substring(index + 1);
      System.Console.WriteLine("FirstName: " + firstName);
      // FirstName: Mosh
      System.Console.WriteLine("LastName: " + lastName);
      // LastName: Hamedani

      var names = fullName.Split(' ');
      System.Console.WriteLine("FirstName: " + names[0]);
      // FirstName: Mosh
      System.Console.WriteLine("FirstName: " + names[1]);
      // FirstName: Hamedani
      System.Console.WriteLine(fullName.Replace("Mosh", "Moshfegh"));
      // Moshfegh Hamedani

      if (String.IsNullOrEmpty(null)) // also works for "".. can trim first
        System.Console.WriteLine("Invalid");
      // Invalid

      if (String.IsNullOrWhiteSpace(" "))
        System.Console.WriteLine("Invalid");
      // Invalid

      var str = "25";
      var age = Convert.ToByte(str);
      System.Console.WriteLine(age);
      // 25

      float price = 29.95f;
      System.Console.WriteLine(price.ToString("C"));
      // $29.95
      // C0 would be $30
    }
  }
}
```

---

## 65: Live Coding: Summarizing Text

### Program.cs

```c#
using System;
using System.Collections.Generic;

namespace _65
{
  class Program
  {
    static void Main(string[] args)
    {
      var sentence = "This is going to be a really really really really really really long text";
      var summary = stringUtility.SummarizeText(sentence, 25);
      System.Console.WriteLine(summary);
    }
  }
}
/*
This is going to be a really...
*/
```

### stringUtility.cs - this makes it public, so we can use it anywhere

```c#
using System;
using System.Collections.Generic;

namespace _65
{
  public class stringUtility
  {
    public static string SummarizeText(string text, int maxLength = 25)
    {
      if (text.Length < maxLength)
        return text;

      var words = text.Split(' ');
      var totalCharacters = 0;
      var summaryWords = new List<string>();
      foreach (var word in words)
      {
        summaryWords.Add(word);
        totalCharacters += word.Length + 1;
        if (totalCharacters > maxLength)
          break;
      }
      return String.Join(" ", summaryWords) + "...";
    }
  }
}
```

---

## 66: String Builder

- Defined in System.Text
- A mutable string
- Easy and fast to create and manipulate strings
- Can NOT search, but….
  - provides append, insert, remove, replace, clear

---

## 67: Demo: String Builder

### program.cs

```c#
using System.Text;
namespace _67

{
  class Program
  {
    static void Main(string[] args)
    {
      var builder = new StringBuilder("Hello World");

      builder
        .Append('-', 10)
        .AppendLine()
        .Append("Header")
        .AppendLine()
        .Append('-', 10)
        .Replace('-', '+')
        .Remove(0, 10)
        .Insert(0, new string('-', 10));

      System.Console.WriteLine(builder);
      // ---d++++++++++
      // Header
      // ++++++++++
      System.Console.WriteLine(builder);
      // ---d++++++++++
      // Header
      // ++++++++++
      System.Console.WriteLine("First Char: " + builder[0]); ;
      // First Char: -
    }
  }
}
```

---

## Quiz 7: Working with Text

Done

---

## 68: Exercises

```c#
using System;
using System.Collections.Generic;

namespace _68
{
  class Program
  {
    static void Main(string[] args)
    {
      Program p = new Program();
      p.Ex1();
      p.Ex2();
      p.Ex3();
      p.Ex4();
      p.Ex5();
    }

    public void Ex1()
    {
      Console.Write("Enter a few numbers (eg 1-2-3-4): ");
      var input = Console.ReadLine();

      var numbers = new List<int>();
      foreach (var number in input.Split('-'))
        numbers.Add(Convert.ToInt32(number));

      numbers.Sort();

      var isConsecutive = true;
      for (var i = 1; i < numbers.Count; i++)
      {
        if (numbers[i] != numbers[i - 1] + 1)
        {
          isConsecutive = false;
          break;
        }
      }

      var message = isConsecutive ? "Consecutive" : "Not Consecutive";
      Console.WriteLine(message);
    }
    /*
    Enter a few numbers (eg 1-2-3-4): 5-5-4-1
    Not Consecutive
    */
    public void Ex2()
    {
      Console.Write("Please enter numbers separated by a hyphen: ");
      var input = Console.ReadLine();
      if (string.IsNullOrWhiteSpace(input))
        return;

      var numbers = new List<int>();
      foreach (var number in input.Split('-'))
        numbers.Add(Convert.ToInt32(number));
      var uniques = new List<int>();
      var includesDuplicates = false;
      foreach (var number in numbers)
      {
        if (!uniques.Contains(number))
          uniques.Add(number);
        else
        {
          includesDuplicates = true;
          break;
        }
      }
      if (includesDuplicates)
        System.Console.WriteLine("Duplicate");
    }
    /*
    Please enter numbers separated by a hyphen: 1-2-5-8
    PS C:\Users\Steve\Desktop\MEGA\Coding\Udemy-C#\Beginner\Files\68> dotnet run
    Please enter numbers separated by a hyphen: 2-2-3
    Duplicate
    */
    public void Ex3()
    {
      Console.Write("Enter a time: ");
      var input = Console.ReadLine();
      if (String.IsNullOrWhiteSpace(input))
      {
        System.Console.WriteLine("Invalid entry");
        return;
      }
      var components = input.Split(':');
      if (components.Length != 2)
      {
        System.Console.WriteLine("Invalid Entry");
        return;
      }
      try
      {
        var hour = Convert.ToInt32(components[0]);
        var minute = Convert.ToInt32(components[1]);
        if (hour >= 0 && hour <= 23 && minute >= 0 && minute <= 59)
          System.Console.WriteLine("ok");
        else
          System.Console.WriteLine("Invalid Time");
      }
      catch (Exception)
      {
        System.Console.WriteLine("Invalid Time");
      }
    }
    /*
    Enter a time: asdf
    Invalid Entry
    PS C:\Users\Steve\Desktop\MEGA\Coding\Udemy-C#\Beginner\Files\68> dotnet run
    Enter a time: 11:11
    ok
    */
    public void Ex4()
    {
      Console.Write("Enter a few words: ");
      var input = Console.ReadLine();
      if (String.IsNullOrWhiteSpace(input))
      {
        System.Console.WriteLine("error");
        return;
      }
      var variableName = "";
      foreach (var word in input.Split(' '))
      {
        var wordWithPascalCase = char.ToUpper(word[0]) + word.ToLower().Substring(1);
        variableName += wordWithPascalCase;
      }
      System.Console.WriteLine(variableName);
    }
    /*
    Enter a few words: Today is not Thursday
    TodayIsNotThursday
    */
    public void Ex5()
    {
      System.Console.WriteLine("Enter a word");
      var input = Console.ReadLine().ToLower();
      var vowels = new List<char>(new char[] { 'a', 'e', 'i', 'o', 'u' });
      var vowelsCount = 0;
      foreach (var character in input)
      {
        if (vowels.Contains(character))
          vowelsCount++;
      }
      System.Console.WriteLine(vowelsCount);
    }
    /*
    Enter a word
    boobies
    4
    */
  }
}
```

---

## 69: Procedural Programming

- **Procedural Programming** - A programming paradigm based on procedural calls
- **Object Oriented Programming** - A programming paradigm based on objects

### Rules

- always separate code that works with the console from code that works with logic
- a method name should explain exactly what it does, without looking at the code

### example

```c#
using System;
namespace example
{
  internal class Program
    {
      public static void Main(string[] args)
      {
        Console.Write("What is your name? ");
        var name = Console.ReadLine();
        var reversed = ReversedName(name);
        Console.WriteLine("Reversed Name: " + reversed)
      }

      public static string ReverseName(string name)
      {
        var array = new char[name.Length];
        for (var i = name.Length; i>0, i--)
          array[name.Length - i] = name[i - 1];

        return new string(array);
      }
    }
}
```

---

## 70: Summary

Done.

---
