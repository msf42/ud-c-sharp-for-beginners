# C# for Beginners

- [Section 01: Introduction](01_Introduction.md)

- [Section 02: Intro to C# and .NET Framework](02_IntroductionToC#AndDotNetFramework.md)

- [Section 03: Primitive Types and Expressions](03_PrimitiveTypesAndExpressions.md)

- [Section 04: Non-Primitive Types](04_NonPrimitiveTypes.md)

- [Section 05: Control Flow](05_ControlFlow.md)

- [Section 06: Arrays and Lists](06_ArraysAndLists.md)

- [Section 07: Working with Dates](07_WorkingWithDates.md)

- [Section 08: Working with Text](08_WorkingWithText.md)

- [Section 09: Working with Files](09_WorkingWithFiles.md)

---
