# 01: Introduction

## Lessons 1 - 5

Just a basic introduction to course and these notes:

### Learning Paths

This course is part of a series of courses that help you master C#. Once you're proficient in C#, then you can focus on specific areas such as web, mobile and game development depending on what your goals and interests are.
Here is the learning path that I recommend to you to get the most out of my courses:

### Essentials

These courses are absolutely essential and you should take them in the following order. Do not skip any of them!

- C# Basics for Beginners (this course)
- [C# Intermediate: Classes, Interfaces and Object-oriented Programming](https://www.udemy.com/csharp-intermediate-classes-interfaces-and-oop/)
- [C# Advanced](https://www.udemy.com/csharp-advanced)

### Mobile Development

Once you master C#, if you'd like to learn how to build mobile apps with C#, you can take the following course:

- [Xamarin Forms](https://www.udemy.com/xamarin-forms-course/)

### Web (Back-end) Development

Alternatively, if you'd like to focus on web application development and become a back-end developer, take the following courses:

- [Entity Framework 6](https://www.udemy.com/entity-framework-tutorial/)
- [ASP.NET MVC 5](https://www.udemy.com/the-complete-aspnet-mvc-5-course/)

### Optional but Recommended

If you'd like to become a serious developer, I highly encourage you to take the following courses too. These courses help you become a better developer and write better code and in less time:

- [Double Your Coding Speed](https://www.udemy.com/visual-studio-tips-tricks/)
- [Clean Code: The Art of Writing Beautiful C# Code](https://www.udemy.com/clean-code/)
- [Unit Testing C# Code](https://www.udemy.com/unit-testing-csharp/)

---
