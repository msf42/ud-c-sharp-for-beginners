# 03: Primitive Types and Expressions

## 14: Introduction

Just basic intro

---

## 15: Variables and Constants

- **Variable**: A name given to a storage location in memory
- **Constant:** An immutable value

### Declaring a Variable

```c#
int number; // type, then identifier
int Number = 1;  // a variable is not required to have a value
const float = 3.14f;  // a constant must have a value
```

### Identifier rules:

- can not begin with a number
- can not include white space
- can not be a reserved keyword
  - may start with `@` instead, such as `@int`

### Naming Conventions

- Camel Case: `firstName`  - Use this for **local variables**
- Pascal Case: `FirstName`  - Use this for **constants**
- Hungarian Notation: `strFirstName` - Don’t use this

![image](images/Lesson015.png)

- When your application is compiled, the compiler will convert the C# datatype to the .NET datatype
- Double is the default datatype for Real Numbers.

### Non-Primitive Types

- String
- Array
- Enum
- Class

---

## 16: Overflowing

```c#
byte number = 255;   // the max value of a byte
number = number + 1; // overflows to 0
```

- C# does not have overflow checking at compile time
- Use `checked` keyword
  - BUT, really isn’t used, because if overflow is a concern, then you should use another datatype, such as short.

```c#
checked
{
  byte number = 255;

  number = number + 1;
}
```

---

## 17: Scope

- Where a variable/constant has meaning, similar to local/global

```C#
{
  byte a = 1;  // a is valid anywhere in this entire block
  {
    byte b = 2;  // b is only valid from here down
    {
      byte c = 3;  // c is only valid here
    }
  }
}
```

---

## 18: Demo: Variables and Constants

```C#
using System;

namespace _18
{
  class Program
  {
    static void Main(string[] args)
    {
      byte number = 2;
      int count = 10;
      float totalPrice = 20.95f; // must add f for float; default is 'double'
      char character = 'A';      // characters use single quotes
      string firstName = "Mosh"; // strings use double quotes
      bool isWorking = true;
      Console.WriteLine(number);            //=> 2
      System.Console.WriteLine(count);      //=> 10
      System.Console.WriteLine(totalPrice); //=> 20.95
      System.Console.WriteLine(character);  //=> A
      System.Console.WriteLine(firstName);  //=> Mosh
      System.Console.WriteLine(isWorking);  //=> True

      System.Console.WriteLine("{0} {1}", byte.MinValue, byte.MaxValue);
      //=> 0  255

      System.Console.WriteLine("{0} {1}", float.MinValue, float.MaxValue);
      //=> -3.402823E+38, 3.402823E+38

      const float Pi = 3.14f; // constants
    }
  }
}
```

Works the same if we simply say “var”:

```c#
    var number = 2;
    var count = 10;
    var totalPrice = 20.95f;
    var character = 'A';
    var firstName = "Mosh";  
    var isWorking = true
```

---

## 19: Type Conversion

### Implicit Type Conversion

Types can be converted with no data loss

```c#
byte b = 1; // stored in one byte as 00000001

int i = b; // stored in 4 bytes as 00000000 00000000 00000000 00000001
```

### Explicit Type Conversion (Casting)

When there s a chance of data loss, the compiler will not do it implicitly, you must tell the compiler that you are aware of the risk and wish to proceed.

```C#
int i = 1;

byte b = i; // won't convert
byte b = (byte)i // will convert
```

### Conversion Between Non-Compatible Types

```C#
string s = "l";

int i = (int)s; // won't compile
```

#### We must use ‘Convert Class’ or ‘Parse Method’

```C#
string s = "l"'

int i = Convert.ToInt32(s);

int j = int.Parse(s);
```

`ToByte()` - converts to byte
`ToInt16()` - converts to short
`ToInt32()` - converts to integer
`ToInt64()` - converts to long

---

## 20: Demo: Type Conversion

```c#
using System;

namespace _20
{
  class Program
  {
    static void Main(string[] args)
    {
      byte b = 1;
      int i = b;
      System.Console.WriteLine(i); //=> 1
      // the reverse would throw an error

      // but we can cast:
      int i2 = 1;
      byte b2 = (byte)i2;
      System.Console.WriteLine(b2); //=> 1
      // if the number were larger, we would lose info:
      int i3 = 400;
      byte b3 = (byte)i3;
      System.Console.WriteLine(b3); //=> 144

      string number = "1234";
      // can not cast
      int i4 = Convert.ToInt32(number);
      System.Console.WriteLine(i4); //=> 1234

      try
      {
        var number5 = "1234";
        byte b5 = Convert.ToByte(number5);
        Console.WriteLine(b5);
      }
      catch (System.Exception)
      {
        Console.WriteLine("The Number Could Not Be Converted to a Byte");
      }
      //=> The Number Could Not Be Converted to a Byte

      try
      {
        string str6 = "true";
        bool b6 = Convert.ToBoolean(str6);
        Console.WriteLine(b6);
      }
      catch (System.Exception)
      {
        throw;
      }
      //=> True
    }
  }
}
```

---

## 21: Operators

### Arithmetic

```c#
+
-
*
/
%
++ (a++ represents a = a + 1)
--
a = 1
b = a++ // in this case, it reads b=a, then b=a+1 (a is still = 1)
// above is "return, then increase"
// below is "increase, then return"
b = ++a // in this case, it reads a = a + 1, then b = a (a is now 2)
```

### Comparison (same as python)

```c#
==
!=
>
>=
<
<=
```

### Assignment

```c#
=
-=
+=
*=
/=
```

### Logical

```c#
&& // and

|| // or

! // not
```

### Bitwise (beyond scope of this course

```c#
& // and

| // or
```

---

## 22: What are Logical Operators?

Pretty straightforward.

---

## 23: Demo: Operators

All basically the same as python

```c#
using System;

namespace _23
{
  class Program
  {
    static void Main(string[] args)
    {
      var a = 10;
      var b = 3;
      Console.WriteLine(a+b); //=> 13
      Console.WriteLine(a/b); //=> 3 (an integer)
      Console.WriteLine((float)a / (float)b); //=> 3.333
      var a2 = 1;
      var b2 = 2;
      var c2 = 3;
      Console.WriteLine(a2 + b2 * c2); //=> 7
      Console.WriteLine(a2 > b2); //=> False
    }
  }
}
```

---

## 24: Comments

### Single Line

```c#
// This is a single line comment
```

### Multi-Line

```c#
/* This is a
Multi Line
Comment */
```

---

## Quiz 2: Primitive Types and Expressions

Passed Quiz!

---

## 25: Summary

Just a 1 min summary

---
