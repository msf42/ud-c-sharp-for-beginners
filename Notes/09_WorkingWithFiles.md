# 09: Working with Files

## 71: Introduction

Done

---

## 72: System.IO Namespace

### System.IO

* File, FileInfo
  * provide methods for creating, copying, deleting, moving, and opening files
  * FileInfo: provides **instance** methods
  * File: provides **static** methods
  * Create(), Copy(), Delete(), Exists(), GetAttributes(), Move(), ReadAllText()
* Directory, DirectoryInfo
  * Directory: provides static methods
  * DirectoryInfo: provides instance methods
  * CreateDirectory(), Delete(), Exists(), GetCurrentDirectory(), GetFiles(), Move(), GetLogicalDrives()
* Path
  * GetDirectoryName(), Get FileName(), GetExtension(), GetTempPath()
* and many more

---

## 73: Demo: File and FileInfo

```c#
using System;
using System.IO;

namespace _73
{
  class Program
  {
    static void Main(string[] args)
    {
      File.Copy(@"c:/temp/myfile.txt", @"c:/temp2/myfile.txt", true);
      // boolean - replace if already exists?
      File.Delete(path);
      if (File.Exists(path))
      {
        // do this
      }
      var content = File.ReadAllText(path);

      var fileInfo = new FileInfo(path);
      fileInfo.CopyTo("....")
      fileInfo.Delete(); // no parameters

      
    }
  }
}
```

---

## 74: Demo: Directory and DirectoryInfo

```csharp
using System;
using System.IO;


namespace _74
{
  class Program
  {
    static void Main(string[] args)
    {
      Directory.CreateDirectory(@"c:/temp/folder1");

      var files = Directory.GetFiles(@"c:/projects/folder", "*.*", SearchOption.AllDirectories)
      foreach (var file in files)
        System.Console.WriteLine(file);
      // returns all files in those folders

      var directories = Directory.GetDirectories(@"c:/folder/folder", "*.*", SearchOption.AllDirectories);
      foreach (directory in directories)
      System.Console.WriteLine(directory);

      Directory.Exists("..."); // returns boolean

      
    }
  }
}

```

---

## 75: Demo: Path

```csharp
using System;
using System.IO;

namespace _75
{
  class Program
  {
    static void Main(string[] args)
    {
      var path = @"C:/home/MEGA/Coding/Udemy-C#/Beginner/files/75/75.csproj";

      var dotIndex = path.IndexOf('.');

      var extension = path.Substring(dotIndex);

      System.Console.WriteLine(Path.GetExtension(path));
      /* output
      .csproj
      */
      System.Console.WriteLine(Path.GetFileName(path));
      System.Console.WriteLine(Path.GetDirectoryName(path));
    }
  }
}

```



---

## 76: Exercises

```csharp
using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;

namespace _76
{
  class Program
  {
    static void Main(string[] args)
    {
      // 1- Write a program that reads a text file and displays the number of words.
      // 2- Write a program that reads a text file and displays the longest word in the file.
      var content = File.ReadAllText(@"warning.txt");
      char[] delimiterChars = {' ', ',', '\t', '\n'};
      string[] words = content.Split(delimiterChars);
  
      var listTot = 0;
      var ans = "";
      for (var i = 0; i < words.Length; i++) {
        // System.Console.WriteLine(words[i]);
        if (words[i].Length > ans.Length) {
          ans = words[i];
        }
        if (words[i].Length > 0) {
          listTot++;
        }
      }
      System.Console.WriteLine(ans); // other-worldly
      System.Console.WriteLine(listTot); // 195
    }
  }
}

```

---

## Quiz 8: Working with Files

Done

---

## 77: Summary

Done

---