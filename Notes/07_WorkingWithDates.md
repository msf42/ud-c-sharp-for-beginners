# 07: Working with Dates

## 58: Introduction

Done

---

## 59: Datetime

```c#
using System;

namespace _59
{
  class Program
  {
    static void Main(string[] args)
    {
      var dateTime = new DateTime(2015, 1, 1);
      var now = DateTime.Now;
      var today = DateTime.Today;
      System.Console.WriteLine("Hour: " + now.Hour);
      // Hour: 10
      System.Console.WriteLine("Minute: " + now.Minute);
      // Minute: 59
      var tomorrow = now.AddDays(1);
      var yesterday = now.AddDays(-1);
      System.Console.WriteLine(now.ToLongDateString());
      // Wednesday, December 18, 2019

      System.Console.WriteLine(now.ToShortDateString());
      // 12/18/2019
      System.Console.WriteLine(now.ToLongTimeString());
      // 10:59:10 AM
      System.Console.WriteLine(now.ToShortTimeString());
      // 10:59 AM
      System.Console.WriteLine(now.ToString());
      // 12/18/2019 10:59:10 AM
      System.Console.WriteLine(now.ToString("yyyy-MM-dd"));
      // 2019-12-18
    }
  }
}

```

---

## 60: Timespan

```c#
using System;

namespace _60
{
  class Program
  {
    static void Main(string[] args)
    {
      var timeSpan = new TimeSpan(1, 2, 3);
      var timeSpan1 = new TimeSpan(1, 0, 0);
      var timeSpan2 = TimeSpan.FromHours(1);
      var start = DateTime.Now;
      var end = DateTime.Now.AddMinutes(2);
      var duration = end - start;
      System.Console.WriteLine("Duration: " + duration);
      // Duration: 00:02:00.0043192

      // Properties
      System.Console.WriteLine("Minutes: " + timeSpan.Minutes);
      // Minutes: 2

      // minutes location only
      System.Console.WriteLine("Total Minutes: " + timeSpan.TotalMinutes);
      // Total Minutes: 62.05
      // converts all to minutes

      System.Console.WriteLine("Add Example: " + timeSpan.Add(TimeSpan.FromMinutes(8)));
      // Add Example: 01:10:03
      System.Console.WriteLine("Add Example: " + timeSpan.Subtract(TimeSpan.FromMinutes(8)));
      // Add Example: 00:54:03

      System.Console.WriteLine("ToString: " + timeSpan.ToString());
      // ToString: 01:02:03

      System.Console.WriteLine("Parse: " + TimeSpan.Parse("01:02:03"));
      // Parse: 01:02:03
    }
  }
}
```

---

## Quiz 6: Dates & Time Quiz

Done

---

## 61: Summary

Done

---
