# 05: Control Flow

## 40: Introduction

Just an intro

---

## 41: Conditional Statements

### 3 Types of Conditional Statements

- If / Else
- Switch / Case
- Conditional Operator `a ? b : c`

![image](images/Lesson041a.png)

![image](images/Lesson041b.png)

![image](images/Lesson041c.png)

![image](images/Lesson041d.png)

---

## 42: Demo: If/Else and Switch/Case

### Program.cs

```c#
using System;

namespace _42
{
  class Program
  {
    static void Main(string[] args)
    {
      Program p = new Program();
      p.IfThen();
      p.Ternary();
      p.SwitchCase();
    }
    public void IfThen()
    {
      int hour = 10;
      if (hour > 0 && hour < 12)
      {
        Console.WriteLine("It's morning.");
      }
      else if (hour >= 12 && hour < 18)
      {
        Console.WriteLine("It's afternoon");
      }
      else
      {
        Console.WriteLine("It's evening.");
      }
    } // Result = "It's morning."

    public void Ternary()
    {
      bool isGoldCustomer = true;

      // the if/else way of doing it
      float price1;
      if (isGoldCustomer)
        price1 = 19.95f;
      else
        price1 = 29.95f;
      Console.WriteLine(price1); // 19.95

      // Conditional Operator way
      float price2 = (isGoldCustomer) ? 19.95f : 29.95f;
      Console.WriteLine(price2); // 19.95
    }


    public void SwitchCase()
    {
      var season = _42.Season.Autumn;
      switch (season)
      {
        case Season.Autumn:
          Console.WriteLine("It's autumn and a beautiful season");
          break;
        case Season.Summer:
          Console.WriteLine("It's a perfect time to go to the beach");
          break;
        default:
          Console.WriteLine("I don't understand");
          break;
      }
    } // It's autumn and a beautiful season
  }
}
```

### Season.cs

```c#
namespace _42
{
  public enum Season
  {
    Spring,
    Summer,
    Autumn,
    Winter
  }
}
```

---

## 43: Exercises

```c#
using System;

namespace _43
{
  class Program
  {
    static void Main(string[] args)
    {
      Program p = new Program();
      p.Ex1();
      p.Ex2();
      p.Ex3();
      p.Ex4();
    }

    public void Ex1()
    {
      System.Console.WriteLine("Exercise 1: Validate a number");
      Console.Write("Enter a number between 1 to 10: ");
      var input = Console.ReadLine();
      var number = Convert.ToInt32(input);
      if (number >= 1 && number <= 10)
        Console.WriteLine("Valid");
      else
        Console.WriteLine("Invalid");
    }
    public void Ex2()
    {
      System.Console.WriteLine("Exercise 2: Print the larger number");
      Console.Write("Enter a number: ");
      var num1 = Convert.ToInt32(Console.ReadLine());
      Console.Write("Enter another number: ");
      var num2 = Convert.ToInt32(Console.ReadLine());
      if (num1 > num2)
        System.Console.WriteLine(num1);
      else
        System.Console.WriteLine(num2);
    }
    public void Ex3()
    {
      System.Console.WriteLine("Exercise 3: Portrait or Landscape?");
      Console.Write("Enter the height of the image:");
      var height = Convert.ToInt32(Console.ReadLine());
      Console.Write("Enter the width of the image:");
      var width = Convert.ToInt32(Console.ReadLine());
      if (width > height)
      {
        Console.WriteLine("The image is landscape");
      }
      else
      {
        Console.WriteLine("The image is portrait");
      }
    }
    public void Ex4()
    {
      System.Console.WriteLine("Exercise 4: Speed Trap");
      Console.Write("Enter the Speed Limit: ");
      var speedLimit = Convert.ToInt32(Console.ReadLine());
      Console.Write("Enter the Car's Speed: ");
      var carSpeed = Convert.ToInt32(Console.ReadLine());
      if (carSpeed < speedLimit)
        System.Console.WriteLine("No Problem");
      else if (carSpeed > (speedLimit + 60))
        System.Console.WriteLine("License Suspended");
      else
        System.Console.WriteLine(((carSpeed - speedLimit) / 5) + " Demerits!");
    }
  }
}
/*
Exercise 1: Validate a number
Enter a number between 1 to 10: 7
Valid

Exercise 2: Print the larger number
Enter a number: 9
Enter another number: 7
9

Exercise 3: Portrait or Landscape?
Enter the height of the image:99
Enter the width of the image:100
The image is landscape

Exercise 4: Speed Trap
Enter the Speed Limit: 55
Enter the Car's Speed: 58
0 Demerits!
*/
```

---

## 44: Iteration Statements

- For
- Foreach
- While
- Do-While
- Break - jumps out of the loop
- Continue - jumps to next iteration

### For

```c#
for (var i = 0; i < 10; i++) // counter, condition, iteration
{
  ...
}
```

### Foreach

```c#
foreach (var number in numbers) // items in a list
{
  ...
}
```

### While

```c#
while (i < 10)
{
  ...
  i++;
}
```

### Do-While

```c#
do
{
  ...
  i++;
} while (i < 10);
```

---

## 45: Demo: For Loops

```c#
using System;

namespace _45
{
  class Program
  {
    static void Main(string[] args)
    {
      for (var i = 1; i <= 10; i++)
      {
        if (i % 2 == 0)
        {
          Console.WriteLine(i);
        }
      }
      // result = 2, 4, 6, 8, 10

      for (var i = 10; i >= 1; i--)
      {
        if (i % 2 == 0)
        {
          Console.WriteLine(i);
        }
      }
      // result = 10, 8, 6, 4, 2
    }
  }
}

```

---

## 46: Demo: Foreach Loops

```c#
using System;

namespace _46
{
  class Program
  {
    static void Main(string[] args)
    {
      var numbers = new int[] {1, 2, 3, 4};
      foreach (var number in numbers)
      {
        Console.WriteLine(number); // 1 2 3 4
      }
    }
  }
}
```

---

## 47: Demo: While Loops

```c#
using System;

namespace _47
{
  class Program
  {
    static void Main(string[] args)
    {
      while (true)
      {
        Console.Write("Type your name: ");
        var input = Console.ReadLine();
        if (!String.IsNullOrWhiteSpace(input))
        {
          System.Console.WriteLine("@Echo: " + input);
          continue;
        }
        break;
      }
    }
  }
}
/*
Type your name: Steve
@Echo: Steve
Type your name: Furches
@Echo: Furches
Type your name:
*/
```

---

## 48: Random Class

### Some improvements on the password generator

```c#
using System;

namespace _48
{
  class Program
  {
    static void Main(string[] args)
    {
      var random = new Random();
      for (var i = 0; i < 10; i++)
        System.Console.WriteLine(random.Next(1, 100));

      const int passwordLength = 10;
      var buffer = new char[passwordLength];
      for (var j = 0; j < passwordLength; j++)
        buffer[j] = ((char)('a' + random.Next(0,26)));
      var password = new string(buffer);
      System.Console.WriteLine(password);
    }
  }
}
/*
97
34
11
91
29
93
25
7
50
74
wlhkjdoxrv
*/
```

---

## Quiz 4

Done

---

## 49: Exercises

```c#
using System;

namespace _49
{
  class Program
  {
    static void Main(string[] args)
    {
      Program p = new Program();
      p.Ex1();
      p.Ex2();
      p.Ex3();
      p.Ex4();
      p.Ex5();
    }

    public void Ex1()
    {
      var counter = 0;
      for (var i = 1; i <=100; i++)
        if (i % 3 == 0)
          counter++;
      System.Console.WriteLine(counter);
    }
    /*
    33
    */

    public void Ex2()
    {
      var counter2 = 0;
      while (true)
      {
        Console.Write("Enter a number or 'ok' to exit: ");
        var input = Console.ReadLine();
        if (input.ToLower() == "ok")
          break;

        counter2 += Convert.ToInt32(input);
      }
      System.Console.WriteLine("Sum total is: " + counter2);
    }
    /*
    Enter a number or 'ok' to exit: 88
    Enter a number or 'ok' to exit: 22
    Enter a number or 'ok' to exit: ok
    Sum total is: 110
    */

    public void Ex3()
    {
      var ans = 1;
      Console.Write("Enter a number: ");
      var numEnt = Convert.ToInt32(Console.ReadLine());
      for (var i = 1; i <=numEnt; i++)
          ans *= i;
      System.Console.WriteLine(ans);
    }
    /*
    Enter a number: 3
    6
    */

    public void Ex4()
    {
      var number = new Random().Next(1, 10);

      Console.WriteLine("Secret is " + number);
      for (var i = 0; i < 4; i++)
      {
        Console.Write("Guess the secret number: ");
        var guess = Convert.ToInt32(Console.ReadLine());

        if (guess == number)
        {
          Console.WriteLine("You won!");
          return;
        }
      }

      Console.WriteLine("You lost!");
    }
    /*
    Secret is 8
    Guess the secret number: 2
    Guess the secret number: 3
    Guess the secret number: 4
    Guess the secret number: 5
    You lost!
    */

    public void Ex5()
    {
      Console.WriteLine("Input a series of numbers, separated by commas: ");
      var input = Console.ReadLine();
      var numbers = input.Split(',');
      var max = Convert.ToInt32(numbers[0]); // the first num
      foreach (var str in numbers)
      {
        var number = Convert.ToInt32(str);
        if (number > max)
          max = number;
      }
      Console.WriteLine(max + " is the max number");
    }
    /*
    Input a series of numbers, separated by commas:
    2, 3, 5
    5 is the max number
    */
  }
}
```

---

## 50: Summary

Done

---
