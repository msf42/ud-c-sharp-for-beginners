﻿using System;
using System.IO;

namespace _75
{
  class Program
  {
    static void Main(string[] args)
    {
      var path = @"C:/home/MEGA/Coding/Udemy-C#/Beginner/files/75/75.csproj";

      var dotIndex = path.IndexOf('.');

      var extension = path.Substring(dotIndex);

      System.Console.WriteLine(Path.GetExtension(path));
      /* output
      .csproj
      */
      System.Console.WriteLine(Path.GetFileName(path));
      System.Console.WriteLine(Path.GetDirectoryName(path));
    }
  }
}
