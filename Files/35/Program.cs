﻿using System;
namespace _35

{
  public enum ShippingMethod
  {
    RegularAirMail = 1,
    RegisteredAirMail = 2,
    Express = 3
  }

  class Program
  {
    static void Main(string[] args)
    {
      var method = ShippingMethod.Express;
      Console.WriteLine((int)method); 
      // 3

      var methodId = 3;
      Console.WriteLine((ShippingMethod)methodId);
      // Express

      Console.WriteLine(method.ToString()); 
      // Express
      // Console.WriteLine always converts to string anyway
     
      var methodName = "Express";
      // parse = convert to a different type
      var shippingMethod = (ShippingMethod)Enum.Parse(typeof(ShippingMethod), methodName); 
      System.Console.WriteLine(shippingMethod);  
      // Express
    }
  }
}
