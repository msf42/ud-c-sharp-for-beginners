﻿using System;

namespace _20
{
  class Program
  {
    static void Main(string[] args)
    {
      byte b = 1;
      int i = b;
      System.Console.WriteLine(i); //=> 1
      // the reverse would throw an error

      // but we can cast:
      int i2 = 1;
      byte b2 = (byte)i2;
      System.Console.WriteLine(b2); //=> 1
      // if the number were larger, we would lose info:
      int i3 = 400;
      byte b3 = (byte)i3;
      System.Console.WriteLine(b3); //=> 144

      string number = "1234";
      // can not cast
      int i4 = Convert.ToInt32(number);
      System.Console.WriteLine(i4); //=> 1234

      try
      {
        var number5 = "1234";
        byte b5 = Convert.ToByte(number5);
        Console.WriteLine(b5);
      }
      catch (System.Exception)
      {
        Console.WriteLine("The Number Could Not Be Converted to a Byte");
      }
      //=> The Number Could Not Be Converted to a Byte

      try
      {
        string str6 = "true";
        bool b6 = Convert.ToBoolean(str6);
        Console.WriteLine(b6);
      }
      catch (System.Exception)
      {

        throw;
      }
      //=> True
    }
  }
}
