﻿using System;

namespace _53
{
  class Program
  {
    static void Main(string[] args)
    {
      var numbers = new[] { 3, 7, 9, 2, 14, 6 };

      // Length
      System.Console.WriteLine("Length: " + numbers.Length); // Length: 6

      // IndexOf()
      var index = Array.IndexOf(numbers, 9);
      System.Console.WriteLine("index of 9 : " + index); // index of 9 : 2

      // Clear()
      Array.Clear(numbers, 0, 2);
      System.Console.WriteLine("Effect of Clear()");
      foreach (var n in numbers)
        System.Console.WriteLine(n);
      /*
      Effect of Clear()
      0
      0
      9
      2
      14
      6
      */
      /*
      Effects of clear on different data types:
        Clear strings => null
        Clear int => 0
        Clear bool => false 
      */

      // Copy()
      int[] another = new int[3];
      Array.Copy(numbers, another, 3);
      System.Console.WriteLine("Effect of Copy()");
      foreach (var n in another)
        System.Console.WriteLine(n);
      /*
      Effect of Copy()
      0
      0
      9
      */

      // Sort()
      Array.Sort(numbers);
      System.Console.WriteLine("Effect of Sort()");
      foreach (var n in numbers)
        System.Console.WriteLine(n);
      /*
      Effect of Sort()
      0
      0
      2
      6
      9
      14
      */


      // Reverse()
      Array.Reverse(numbers);
      System.Console.WriteLine("Effects of reverse()");
      foreach (var n in numbers)
        System.Console.WriteLine(n);
      /*
      Effects of reverse()
      14
      9
      6
      2
      0
      0
      */
    }
  }
}
