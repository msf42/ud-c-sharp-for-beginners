﻿using System;

namespace _37
{
  class Program
  {
    static void Main(string[] args)
    {
      var a = 10;
      var b = a;
      b++;
      Console.WriteLine(string.Format("a: {0}, b:{1}", a, b));
      // output: a: 10, b:11
      // because integers are value types, var b is a copy

// an array is a reference type, so array2 just points to the same array
      var array1 = new int[3]{1, 2, 3};
      var array2 = array1;
      array2[0] = 0;
      Console.WriteLine(string.Format("array1: {0}, array2: {1}", array1[0], array2[0]));
      // output: array1: 0, array2: 0
      // because arrays are reference types, both are changed
    }
  }
}
