﻿using System;

namespace _23
{
	class Program
	{
		static void Main(string[] args)
		{
			var a = 10;
			var b = 3;
			Console.WriteLine(a+b); //=> 13
			Console.WriteLine(a/b); //=> 3
			Console.WriteLine((float)a / (float)b); //=> 3.333
			var a2 = 1;
			var b2 = 2;
			var c2 = 3;
			Console.WriteLine(a2 + b2 * c2); //=> 7
			Console.WriteLine(a2 > b2); //=> False
		}
	}
}
