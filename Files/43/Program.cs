﻿using System;

namespace _43
{
  class Program
  {
    static void Main(string[] args)
    {
      Program p = new Program();
      p.Ex1();
      p.Ex2();
      p.Ex3();
      p.Ex4();
    }

    public void Ex1()
    {
      System.Console.WriteLine("Exercise 1: Validate a number");
      Console.Write("Enter a number between 1 to 10: ");
      var input = Console.ReadLine();
      var number = Convert.ToInt32(input);
      if (number >= 1 && number <= 10)
        Console.WriteLine("Valid");
      else
        Console.WriteLine("Invalid");
    }
    public void Ex2()
    {
      System.Console.WriteLine("Exercise 2: Print the larger number");
      Console.Write("Enter a number: ");
      var num1 = Convert.ToInt32(Console.ReadLine());
      Console.Write("Enter another number: ");
      var num2 = Convert.ToInt32(Console.ReadLine());
      if (num1 > num2)
        System.Console.WriteLine(num1);
      else
        System.Console.WriteLine(num2);
    }
    public void Ex3()
    {
      System.Console.WriteLine("Exercise 3: Portrait or Landscape?");
      Console.Write("Enter the height of the image:");
      var height = Convert.ToInt32(Console.ReadLine());
      Console.Write("Enter the width of the image:");
      var width = Convert.ToInt32(Console.ReadLine());
      if (width > height)
      {
        Console.WriteLine("The image is landscape");
      }
      else
      {
        Console.WriteLine("The image is portrait");
      }
    }
    public void Ex4()
    {
      System.Console.WriteLine("Exercise 4: Speed Trap");
      Console.Write("Enter the Speed Limit: ");
      var speedLimit = Convert.ToInt32(Console.ReadLine());
      Console.Write("Enter the Car's Speed: ");
      var carSpeed = Convert.ToInt32(Console.ReadLine());
      if (carSpeed < speedLimit)
        System.Console.WriteLine("No Problem");
      else if (carSpeed > (speedLimit + 60))
        System.Console.WriteLine("License Suspended");
      else
        System.Console.WriteLine(((carSpeed - speedLimit) / 5) + " Demerits!");
    }
  }
}
/*
Exercise 1: Validate a number
Enter a number between 1 to 10: 7
Valid

Exercise 2: Print the larger number
Enter a number: 9
Enter another number: 7
9

Exercise 3: Portrait or Landscape?
Enter the height of the image:99
Enter the width of the image:100
The image is landscape

Exercise 4: Speed Trap
Enter the Speed Limit: 55
Enter the Car's Speed: 58
0 Demerits!
*/
