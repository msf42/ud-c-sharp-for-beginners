﻿using System;

namespace _48
{
  class Program
  {
    static void Main(string[] args)
    {
      var random = new Random();
      for (var i = 0; i < 10; i++)
        System.Console.WriteLine(random.Next(1, 100));
      
      const int passwordLength = 10;
      var buffer = new char[passwordLength];
      for (var j = 0; j < passwordLength; j++)
        buffer[j] = ((char)('a' + random.Next(0,26)));
      var password = new string(buffer);
      System.Console.WriteLine(password);
    }
  }
}
/*
97
34
11
91
29
93
25
7
50
74
wlhkjdoxrv
*/
