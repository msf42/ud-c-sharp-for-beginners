﻿using System;

namespace _64
{
  class Program
  {
    static void Main(string[] args)
    {
      var fullName = "Mosh Hamedani ";
      System.Console.WriteLine("Trim: '{0}'", fullName.Trim());
      // Trim: 'Mosh Hamedani'
      System.Console.WriteLine("ToUpper: '{0}'", fullName.ToUpper());
      // ToUpper: 'MOSH HAMEDANI '

      var index = fullName.IndexOf(' ');
      var firstName = fullName.Substring(0, index);
      var lastName = fullName.Substring(index + 1);
      System.Console.WriteLine("FirstName: " + firstName);
      // FirstName: Mosh
      System.Console.WriteLine("LastName: " + lastName);
      // LastName: Hamedani

      var names = fullName.Split(' ');
      System.Console.WriteLine("FirstName: " + names[0]);
      // FirstName: Mosh
      System.Console.WriteLine("FirstName: " + names[1]);
      // FirstName: Hamedani
      System.Console.WriteLine(fullName.Replace("Mosh", "Moshfegh"));
      // Moshfegh Hamedani

      if (String.IsNullOrEmpty(null)) // also works for "".. can trim first
        System.Console.WriteLine("Invalid");
      // Invalid

      if (String.IsNullOrWhiteSpace(" "))
        System.Console.WriteLine("Invalid");
      // Invalid

      var str = "25";
      var age = Convert.ToByte(str);
      System.Console.WriteLine(age);
      // 25

      float price = 29.95f;
      System.Console.WriteLine(price.ToString("C"));
      // $29.95
      // C0 would be $30
    }
  }
}
