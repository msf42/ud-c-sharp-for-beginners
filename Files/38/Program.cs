﻿using System;

namespace _38
{
  public class Person
  {
    public int Age;
  }
  
  class Program
  {
    static void Main(string[] args)
    {
      var number = 1;
      Increment(number); // creates, then destroys.. it is a copy
      Console.WriteLine(number); // number is still 1

      var person = new Person() {Age = 20}; // since this is a reference type
      MakeOld(person);  // person will now be 30
      Console.WriteLine(person.Age); // 30
    }
    public static void Increment(int number)
    {
      number += 10;
    }
    public static void MakeOld(Person person)
    {
      person.Age += 10;
    }
  }
}
