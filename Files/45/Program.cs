﻿using System;

namespace _45
{
  class Program
  {
    static void Main(string[] args)
    {
      for (var i = 1; i <= 10; i++)
      {
        if (i % 2 == 0)
        {
          Console.WriteLine(i);
        }
      }
      // result = 2, 4, 6, 8, 10

      for (var i = 10; i >= 1; i--)
      {
        if (i % 2 == 0)
        {
          Console.WriteLine(i);
        }
      }
      // result = 10, 8, 6, 4, 2
    }
  }
}
