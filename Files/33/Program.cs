﻿using System;
namespace _33
{
  class Program
  {
    static void Main(string[] args)
    {
      var firstName = "Mosh";
      var lastName = "Hamedani";
      var fullName = firstName + " " + lastName;
      var myFullName = string.Format("My name is {0} {1}", firstName, lastName);
      System.Console.WriteLine(myFullName);
      // My name is Mosh Hamedani

      var names = new string[3] {"John", "Jack", "Mary"};
      var formattedNames = string.Join(",", names);
      System.Console.WriteLine(formattedNames);
      // John,Jack,Mary
      
      var text1 = "Hi John\nLook into the following paths\nc:\\folder1\\folder2\nc:\\folder3\\folder4";
      
      var text2 = @"Hi John
      Look into the following paths
      c:\\folder1\\folder2
      c:\\folder3\\folder4";
      System.Console.WriteLine(text1);
      /*
      Hi John
      Look into the following paths
      c:\folder1\folder2
      c:\folder3\folder4
      */

      System.Console.WriteLine(text2);
      /*
      Hi John
      Look into the following paths
      c:\\folder1\\folder2
      c:\\folder3\\folder4
      */
    }
  }
}