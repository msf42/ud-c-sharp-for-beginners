﻿using System;
using System.Collections.Generic;

namespace _55
{
  class Program
  {
    static void Main(string[] args)
    {
      // When working with a generic type, <type> must be used
      var numbers = new List<int>() { 1, 2, 3, 4 };
      numbers.Add(1); // add one item
      numbers.AddRange(new int[3] { 5, 6, 7 }); // add a range

      foreach (var number in numbers)
        System.Console.WriteLine(number);
      /* 1 2 3 4 1 5 6 7 */

      System.Console.WriteLine();

      System.Console.WriteLine("index of 1: " + numbers.IndexOf(1)); // first
      /* index of 1: 0 */

      System.Console.WriteLine("last index of 1: " + numbers.LastIndexOf(1));  // last
      /* last index of 1: 4 */

      System.Console.WriteLine("Count: " + numbers.Count); // count
      /* Count: 8 */

      for (var i = 0; i < numbers.Count; i++) // remove 1s
      {
        if (numbers[i] == 1)
          numbers.Remove(numbers[i]);
      }

      foreach (var number in numbers) // write them again
        System.Console.WriteLine(number);
      /* 2 3 4 5 6 7 */

      numbers.Clear(); // clear the list
      System.Console.WriteLine("Count: " + numbers.Count);
      /* Count: 0 */
    }
  }
}
