﻿using System;

namespace _47
{
  class Program
  {
    static void Main(string[] args)
    {
      while (true)
      {
        Console.Write("Type your name: ");
        var input = Console.ReadLine();
        if (!String.IsNullOrWhiteSpace(input))
        {
          System.Console.WriteLine("@Echo: " + input);
          continue;
        }
        break;
      }
    }
  }
}
/*
Type your name: Steve
@Echo: Steve
Type your name: Furches
@Echo: Furches
Type your name: 
*/