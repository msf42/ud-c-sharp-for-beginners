﻿using System;

namespace _18
{
	class Program
	{
		static void Main(string[] args)
		{
			byte number = 2;
			int count = 10;
			float totalPrice = 20.95f; // must add f for float; default is 'double'
			char character = 'A';      // characters use single quotes
			string firstName = "Mosh"; // strings use double quotes
			bool isWorking = true;
			Console.WriteLine(number);            // 2
			System.Console.WriteLine(count);      // 10
			System.Console.WriteLine(totalPrice); // 20.95
			System.Console.WriteLine(character);  // A
			System.Console.WriteLine(firstName);  // Mosh
			System.Console.WriteLine(isWorking);  // True

			System.Console.WriteLine("{0} {1}", byte.MinValue, byte.MaxValue); 
			// 0  255
			
			System.Console.WriteLine("{0} {1}", float.MinValue, float.MaxValue); 
			// -3.402823E+38, 3.402823E+38

			const float Pi = 3.14f; // constants
			System.Console.WriteLine(Pi); // 3.14
		}
	}
}
/* Output:
2
10
20.95
A
Mosh
True
*/