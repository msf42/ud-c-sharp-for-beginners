﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;

namespace _76
{
  class Program
  {
    static void Main(string[] args)
    {
      // 1- Write a program that reads a text file and displays the number of words.
      //2- Write a program that reads a text file and displays the longest word in the file.
      var content = File.ReadAllText(@"warning.txt");
      char[] delimiterChars = {' ', ',', '\t', '\n'};
      string[] words = content.Split(delimiterChars);
  
      var listTot = 0;
      var ans = "";
      for (var i = 0; i < words.Length; i++) {
        // System.Console.WriteLine(words[i]);
        if (words[i].Length > ans.Length) {
          ans = words[i];
        }
        if (words[i].Length > 0) {
          listTot++;
        }
      }
      System.Console.WriteLine(ans); // other-worldly
      System.Console.WriteLine(listTot); // 195
    }
  }
}
