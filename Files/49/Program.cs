﻿using System;

namespace _49
{
  class Program
  {
    static void Main(string[] args)
    {
      Program p = new Program();
      p.Ex1();
      p.Ex2();
      p.Ex3();
      p.Ex4();
      p.Ex5();
    }
    
    public void Ex1()
    {
      var counter = 0;
      for (var i = 1; i <=100; i++)
        if (i % 3 == 0)
          counter++;
      System.Console.WriteLine(counter);
    }
    /*
    33
    */

    public void Ex2()
    {
      var counter2 = 0;
      while (true)
      {
        Console.Write("Enter a number or 'ok' to exit: ");
        var input = Console.ReadLine();
        if (input.ToLower() == "ok")
          break;
        
        counter2 += Convert.ToInt32(input);
      }
      System.Console.WriteLine("Sum total is: " + counter2);
    }
    /*
    Enter a number or 'ok' to exit: 88
    Enter a number or 'ok' to exit: 22
    Enter a number or 'ok' to exit: ok
    Sum total is: 110
    */

    public void Ex3()
    {
      var ans = 1;
      Console.Write("Enter a number: ");
      var numEnt = Convert.ToInt32(Console.ReadLine());
      for (var i = 1; i <=numEnt; i++)
          ans *= i;
      System.Console.WriteLine(ans);
    }
    /*
    Enter a number: 3
    6
    */

    public void Ex4()
    {
      var number = new Random().Next(1, 10);

      Console.WriteLine("Secret is " + number);
      for (var i = 0; i < 4; i++)
      {
        Console.Write("Guess the secret number: ");
        var guess = Convert.ToInt32(Console.ReadLine());

        if (guess == number)
        {
          Console.WriteLine("You won!");
          return;
        }
      }

      Console.WriteLine("You lost!");
    }
    /*
    Secret is 8
    Guess the secret number: 2
    Guess the secret number: 3
    Guess the secret number: 4
    Guess the secret number: 5
    You lost!
    */

    public void Ex5()
    {
      Console.WriteLine("Input a series of numbers, separated by commas: ");
      var input = Console.ReadLine();
      var numbers = input.Split(',');
      var max = Convert.ToInt32(numbers[0]); // the first num
      foreach (var str in numbers)
      {
        var number = Convert.ToInt32(str);
        if (number > max)
          max = number;
      }
      Console.WriteLine(max + " is the max number");
    }
    /*
    Input a series of numbers, separated by commas: 
    2, 3, 5
    5 is the max number
    */
  }
}
