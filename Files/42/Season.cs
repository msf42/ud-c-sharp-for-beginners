namespace _42
{
  public enum Season
  {
    Spring,
    Summer,
    Autumn,
    Winter
  }
}