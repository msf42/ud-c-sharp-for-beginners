﻿using System;

namespace _42
{
  class Program
  {
    static void Main(string[] args)
    {
      Program p = new Program();
      p.IfThen();
      p.Ternary();
      p.SwitchCase();
    }
    public void IfThen()
    {
      int hour = 10;
      if (hour > 0 && hour < 12)
      {
        Console.WriteLine("It's morning.");
      }
      else if (hour >= 12 && hour < 18)
      {
        Console.WriteLine("It's afternoon");
      }
      else
      {
        Console.WriteLine("It's evening.");
      }
    } // Result = "It's morning."

    public void Ternary()
    {
      bool isGoldCustomer = true;

      // the if/else way of doing it
      float price1;
      if (isGoldCustomer)
        price1 = 19.95f;
      else
        price1 = 29.95f;
      Console.WriteLine(price1); // 19.95

      // Conditional Operator way
      float price2 = (isGoldCustomer) ? 19.95f : 29.95f;
      Console.WriteLine(price2); // 19.95
    }


    public void SwitchCase()
    {
      var season = _42.Season.Autumn;
      switch (season)
      {
        case Season.Autumn:
          Console.WriteLine("It's autumn and a beautiful season");
          break;
        case Season.Summer:
          Console.WriteLine("It's a perfect time to go to the beach");
          break;
        default:
          Console.WriteLine("I don't understand");
          break;
      }
    } // It's autumn and a beautiful season
  }
}
