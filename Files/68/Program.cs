﻿using System;
using System.Collections.Generic;

namespace _68
{
  class Program
  {
    static void Main(string[] args)
    {
      Program p = new Program();
      p.Ex1();
      p.Ex2();
      p.Ex3();
      p.Ex4();
      p.Ex5();
    }

    public void Ex1()
    {
      Console.Write("Enter a few numbers (eg 1-2-3-4): ");
      var input = Console.ReadLine();

      var numbers = new List<int>();
      foreach (var number in input.Split('-'))
        numbers.Add(Convert.ToInt32(number));

      numbers.Sort();

      var isConsecutive = true;
      for (var i = 1; i < numbers.Count; i++)
      {
        if (numbers[i] != numbers[i - 1] + 1)
        {
          isConsecutive = false;
          break;
        }
      }

      var message = isConsecutive ? "Consecutive" : "Not Consecutive";
      Console.WriteLine(message);
    }
    /*
    Enter a few numbers (eg 1-2-3-4): 5-5-4-1
    Not Consecutive
    */
    public void Ex2()
    {
      Console.Write("Please enter numbers separated by a hyphen: ");
      var input = Console.ReadLine();
      if (string.IsNullOrWhiteSpace(input))
        return;

      var numbers = new List<int>();
      foreach (var number in input.Split('-'))
        numbers.Add(Convert.ToInt32(number));
      var uniques = new List<int>();
      var includesDuplicates = false;
      foreach (var number in numbers)
      {
        if (!uniques.Contains(number))
          uniques.Add(number);
        else
        {
          includesDuplicates = true;
          break;
        }
      }
      if (includesDuplicates)
        System.Console.WriteLine("Duplicate");
    }
    /*
    Please enter numbers separated by a hyphen: 1-2-5-8
    PS C:\Users\Steve\Desktop\MEGA\Coding\Udemy-C#\Beginner\Files\68> dotnet run
    Please enter numbers separated by a hyphen: 2-2-3
    Duplicate
    */
    public void Ex3()
    {
      Console.Write("Enter a time: ");
      var input = Console.ReadLine();
      if (String.IsNullOrWhiteSpace(input))
      {
        System.Console.WriteLine("Invalid entry");
        return;
      }
      var components = input.Split(':');
      if (components.Length != 2)
      {
        System.Console.WriteLine("Invalid Entry");
        return;
      }
      try
      {
        var hour = Convert.ToInt32(components[0]);
        var minute = Convert.ToInt32(components[1]);
        if (hour >= 0 && hour <= 23 && minute >= 0 && minute <= 59)
          System.Console.WriteLine("ok");
        else
          System.Console.WriteLine("Invalid Time");
      }
      catch (Exception)
      {
        System.Console.WriteLine("Invalid Time");
      }
    }
    /*
    Enter a time: asdf
    Invalid Entry
    PS C:\Users\Steve\Desktop\MEGA\Coding\Udemy-C#\Beginner\Files\68> dotnet run
    Enter a time: 11:11
    ok
    */
    public void Ex4()
    {
      Console.Write("Enter a few words: ");
      var input = Console.ReadLine();
      if (String.IsNullOrWhiteSpace(input))
      {
        System.Console.WriteLine("error");
        return;
      }
      var variableName = "";
      foreach (var word in input.Split(' '))
      {
        var wordWithPascalCase = char.ToUpper(word[0]) + word.ToLower().Substring(1);
        variableName += wordWithPascalCase;
      }
      System.Console.WriteLine(variableName);
    }
    /*
    Enter a few words: Today is not Thursday
    TodayIsNotThursday
    */
    public void Ex5()
    {
      System.Console.WriteLine("Enter a word");
      var input = Console.ReadLine().ToLower();
      var vowels = new List<char>(new char[] { 'a', 'e', 'i', 'o', 'u' });
      var vowelsCount = 0;
      foreach (var character in input)
      {
        if (vowels.Contains(character))
          vowelsCount++;
      }
      System.Console.WriteLine(vowelsCount);
    }
    /*
    Enter a word
    boobies
    4
    */
  }
}
