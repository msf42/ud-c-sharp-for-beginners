﻿using System;
using System.Collections.Generic;

namespace _65
{
  class Program
  {
    static void Main(string[] args)
    {
      var sentence = "This is going to be a really really really really really really long text";
      var summary = stringUtility.SummarizeText(sentence, 25);
      System.Console.WriteLine(summary);
    }
  }
}