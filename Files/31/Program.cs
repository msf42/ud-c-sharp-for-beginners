﻿using System;

namespace _31
{
  class Program
  {
    static void Main(string[] args)
    {
      var numbers = new int[3];
      numbers[0] = 1;
      System.Console.WriteLine(numbers[0]); //=> 1
      System.Console.WriteLine(numbers[1]); //=> 0
      System.Console.WriteLine(numbers[2]); //=> 0

      var flags = new bool[3];
      flags[0] = true;
      System.Console.WriteLine(flags[0]); //=> True
      System.Console.WriteLine(flags[1]); //=> False
      System.Console.WriteLine(flags[2]); //=> False

      var names = new string[3] { "Jack", "John", "Mary"};
      
    }
  }
}
