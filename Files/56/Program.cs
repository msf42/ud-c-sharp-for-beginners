﻿using System;
using System.Collections.Generic;

namespace _43
{
  class Program
  {
    static void Main(string[] args)
    {
      Program p = new Program();
      // p.Ex1();
      // p.Ex2();
      // p.Ex3();
      // p.Ex4();
      p.Ex5();
    }

    public void Ex1()
    {
      {
        var names = new List<string>();
        while (true)
        {
          Console.Write("Type a name (or hit ENTER to quit): ");
          var input = Console.ReadLine();
          if (String.IsNullOrWhiteSpace(input))
            break;
          names.Add(input);
        }

        var listLength = names.Count;
        // System.Console.WriteLine(listLength);
        switch (listLength)
        {
          case 0:
            System.Console.WriteLine("No one likes your post");
            break;
          case 1:
            System.Console.WriteLine(names[0] + " likes your post.");
            break;
          case 2:
            System.Console.WriteLine(names[0] + " and " + names[1] + " like your post.");
            break;
          default:
            System.Console.WriteLine(names[0] + ", " + names[1] + ", and " + (listLength - 2) + " others like your post.");
            break;
        }
      }
    }
    /*
    Type a name(or hit ENTER to quit) : steve
    Type a name(or hit ENTER to quit): annie
    Type a name(or hit ENTER to quit): leo
    Type a name(or hit ENTER to quit):
    steve, annie, and 1 others like your post.
    */

    public void Ex2()
    {
      Console.Write("What's your name? "); // ask name
      var name = Console.ReadLine();      // read it
      var array = new char[name.Length]; // arr w/length of name

      for (var i = name.Length; i > 0; i--)
        // for each letter, while > 0, i--
        array[name.Length - i] = name[i - 1];

      // add each to the array in rev order
      var reversed = new string(array);
      Console.WriteLine("Reversed name: " + reversed);
    }
    /*
    What's your name? Zack
    Reversed name: kcaZ
    */

    public void Ex3()
    {
      var numbers = new List<int>();

      while (numbers.Count < 5)
      {
        Console.Write("Enter a number: ");
        var number = Convert.ToInt32(Console.ReadLine());
        if (numbers.Contains(number))
        {
          Console.WriteLine("You've previously entered " + number);
          continue;
        }
        numbers.Add(number);
      }
      numbers.Sort();

      foreach (var number in numbers)
        Console.WriteLine(number);
    }
    /*
    Enter a number: 1
    Enter a number: 2
    Enter a number: 3
    Enter a number: 3
    You've previously entered 3
    Enter a number: 4
    Enter a number: 5
    1
    2
    3
    4
    5
    */

    public void Ex4()
    {
      var numbers = new List<int>();

      while (true)
      {
        Console.Write("Enter a number (or 'Quit' to exit): ");
        var input = Console.ReadLine();
        if (input.ToLower() == "quit")
          break;
        numbers.Add(Convert.ToInt32(input));
      }

      var uniques = new List<int>();
      foreach (var number in numbers)
      {
        if (!uniques.Contains(number))
          uniques.Add(number);
      }
      Console.WriteLine("Unique numbers:");

      foreach (var number in uniques)
        Console.WriteLine(number);
    }
    /*
    Enter a number (or 'Quit' to exit): 1
    Enter a number (or 'Quit' to exit): 2
    Enter a number (or 'Quit' to exit): 2
    Enter a number (or 'Quit' to exit): 3
    Enter a number (or 'Quit' to exit): 3
    Enter a number (or 'Quit' to exit): 3
    Enter a number (or 'Quit' to exit): 4
    Enter a number (or 'Quit' to exit): quit
    Unique numbers:
    1
    2
    3
    4
    */

    public void Ex5()
    {
      string[] elements;
      while (true)
      {
        Console.Write("Enter a list of comma-separated numbers: ");
        var input = Console.ReadLine();
        if (!String.IsNullOrWhiteSpace(input)) // if ans not null
        {
          elements = input.Split(','); // split on commas
          if (elements.Length >= 5) // test list
            break;
        }
        Console.WriteLine("Invalid List");
      }

      var numbers = new List<int>();  // blank list

      foreach (var number in elements)
        numbers.Add(Convert.ToInt32(number)); // add nums to list

      var smallests = new List<int>();
      while (smallests.Count < 3)
      {
        // Assume the first number is the smallest
        var min = numbers[0];
        foreach (var number in numbers)
        {
          if (number < min)
            min = number;
        }
        smallests.Add(min);
        numbers.Remove(min);
      }

      Console.WriteLine("The 3 smallest numbers are: ");

      foreach (var number in smallests)
        Console.WriteLine(number);
    }
    /*
    Enter a list of comma-separated numbers: 2, 3, 9, 22
    Invalid List
    Enter a list of comma-separated numbers: 2, 5, 3, 33, 99, 123
    The 3 smallest numbers are:
    2
    3
    5
    */
  }
}
