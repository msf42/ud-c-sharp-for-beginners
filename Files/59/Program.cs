﻿using System;

namespace _59
{
  class Program
  {
    static void Main(string[] args)
    {
      var dateTime = new DateTime(2015, 1, 1);
      var now = DateTime.Now;
      var today = DateTime.Today;
      System.Console.WriteLine("Hour: " + now.Hour);
      // Hour: 10
      System.Console.WriteLine("Minute: " + now.Minute);
      // Minute: 59
      var tomorrow = now.AddDays(1);
      var yesterday = now.AddDays(-1);
      System.Console.WriteLine(now.ToLongDateString());
      // Wednesday, December 18, 2019

      System.Console.WriteLine(now.ToShortDateString());
      // 12/18/2019
      System.Console.WriteLine(now.ToLongTimeString());
      // 10:59:10 AM
      System.Console.WriteLine(now.ToShortTimeString());
      // 10:59 AM
      System.Console.WriteLine(now.ToString());
      // 12/18/2019 10:59:10 AM
      System.Console.WriteLine(now.ToString("yyyy-MM-dd"));
      // 2019-12-18
    }
  }
}
